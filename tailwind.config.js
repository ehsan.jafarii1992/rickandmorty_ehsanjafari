/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        white2: "#e9e9e9",
        white3: "#f1f1f1",
        plateGray: "rgb(194, 192, 192)",
        plateGray2: "rgb(150, 147, 147)",
      },
      backdropBlur: {
        1: "1px",
      },
      zIndex: {
        100: "100",
        1000: "1000",
      },
    },
  },
  plugins: [],
}
