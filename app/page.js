"use client"
import Layout from "@/components/organisms/layout"
import HomePage from "@/components/molecules/home"
import { QueryClient, QueryClientProvider } from "react-query";

export default function Home() {
  const client = new QueryClient();
  return (
    <QueryClientProvider client={client}>
      <Layout>
        <HomePage/>
    </Layout>
    </QueryClientProvider>
  )
}
