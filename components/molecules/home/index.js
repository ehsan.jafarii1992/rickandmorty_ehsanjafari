import { useQuery } from "react-query";
import axios from "@/config/axios";
import Characters from "./characters";
import Text from "@/components/atom/Text";
import { useState } from "react";
const HomePage = () => {
  const { data } = useQuery(["rick"], () =>
    axios
      .get(
        `graphql?query=${encodeURIComponent(`{
                 characters(page: 1) {
                   results {
                     name
                     status
                     image
                     type
                   }
               }
             }`)}`
      )
      .then((res) => res.data.data.characters.results)
  );


  return (
    <div className="mt-[3rem]">
      <Text className="text-2xl sm:text-4xl lg:text-6xl font-bold text-center">
        Rick and Morty
      </Text>
      <Characters data={data} />
    </div>
  );
};

export default HomePage;
