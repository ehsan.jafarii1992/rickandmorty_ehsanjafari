import Text from "@/components/atom/Text";
import Classes from "./style.module.css";
import Image from "next/image";
import ModalChrctr from "../modalChrctr";
import { useState } from "react";
const Character = ({ data }) => {
  const [open, setOpen] = useState(false);
  const [item, setItem] = useState();
  return (
    <>
      <div
        key={data.name}
        className={Classes.mainBox}
        onClick={() => {
          setItem(data);
          setOpen(state => !state);
        }}
      >
        <div className={Classes.image}>
          <Image
            src={data.image}
            layout="fill"
            objectFit="fill"
            className="rounded-t-xl"
          />
        </div>
        <div className={Classes.textBox}>
          <Text className={Classes.title}>{data.name}</Text>
          <Text className={Classes.status}>{data.status}</Text>
        </div>
      </div>
      <ModalChrctr open={open} setOpen={setOpen} item={item} />
    </>
  );
};
export default Character;
