import Classes from "./style.module.css";
import Character from "./character";

const Characters = ({ data }) => {
  return (
    <div className={Classes.Characters}>
      {data?.map((item) => (
        <Character data={item} />
      ))}
    </div>
  );
};

export default Characters;
