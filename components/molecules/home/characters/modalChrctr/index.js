import Modal from "@/components/molecules/modal";
import Clsses from "./style.module.css";
import Image from "next/image";
import Text from "@/components/atom/Text";
const ModalChrctr = ({ open, setOpen, item }) => {
  return (
    <Modal open={open} onClose={() => setOpen(false)}>
      <div className={Clsses.mainBox}>
        
        <div className={Clsses.image}>
          <Image
            src={item?.image}
            layout="fill"
            objectFit="cover"
            className="rounded-2xl"
          />
        </div>
        <div className={Clsses.textBox}>
            <div>
            <Text className={Clsses.title}>name : </Text>
            <Text className={Clsses.value}>{item?.name}</Text>
                
            </div>
            <div>
            <Text className={Clsses.title}>status : </Text>
            <Text className={Clsses.value}>{item?.status}</Text>
                
            </div>
            <div>
            <Text className={Clsses.title}>type : </Text>
            <Text className={Clsses.value}>{item?.type || "_"}</Text>
                
            </div>
        </div>
        
      </div>
    </Modal>
  );
};

export default ModalChrctr;
