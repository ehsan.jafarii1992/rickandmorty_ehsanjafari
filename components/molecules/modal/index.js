"use client"
import React from "react";
import useLockBodyScroll from "@/hooks/useLockBodyScroll";
import Portal from "@/components/atom/portal";
import classes from "./style.module.css";
import clsx from "clsx";
export default function Modal({
  open,
  onClose,
  locked,
  className,
  children,
  parentClass,
}) {
  useLockBodyScroll(open);
  const [active, setActive] = React.useState(false);
  const backdrop = React.useRef(null);

  React.useEffect(() => {
    const { current } = backdrop;

    const transitionEnd = () => setActive(open);

    const keyHandler = (e) =>
      !locked && [27].indexOf(e.which) >= 0 && onClose();

    const clickHandler = (e) => !locked && e.target === current && onClose();

    if (current) {
      current.addEventListener("transitionend", transitionEnd);
      current.addEventListener("click", clickHandler);
      window.addEventListener("keyup", keyHandler);
    }

    if (open) {
      window.setTimeout(() => {
        document.activeElement.blur();
        setActive(open);
        document.querySelector("#__next")?.setAttribute("inert", "true");
      }, 10);
    }

    return () => {
      if (current) {
        current.removeEventListener("transitionend", transitionEnd);
        current.removeEventListener("click", clickHandler);
      }

      document.querySelector("#__next")?.removeAttribute("inert");
      window.removeEventListener("keyup", keyHandler);
    };
  }, [open, locked, onClose]);

  return (
    <React.Fragment>
      {(open || active) && (
        <Portal className="modal-portal">
          <div
            ref={backdrop}
            className={clsx(
              classes.modalBox,
              active && open && classes.activeModal,
              parentClass
            )}
          >
            <div
              className={clsx(
                classes.innerModal,
                active && open && classes.innerModalActive,
                className
              )}
            >
              {children}
            </div>
          </div>
        </Portal>
      )}
    </React.Fragment>
  );
}
