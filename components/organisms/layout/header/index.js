import Link from "next/link";
import classes from "./style.module.css";
// import NavBar from "@/helpers/navBar/navBar";
// import useWindowSize from "@/hooks/useWindowSize";
import Image from "next/image";
import Logo from "@/public/images/logo.png"
const Header = () => {
  // const { lg } = useWindowSize();

  return (
    <div className={classes.header}>
      <div className={classes.logoBox}>
      <div className={classes.logo}>
          <Image src={Logo} alt="logo" layout="fill" objectFit="cover" />
        </div>
        <span className={classes.dot}></span>
      </div>
      {/* {lg ? (
        <div className={classes.nav}>
          {NavBar.map((item) => (
            <Link href={item.href}>{item.lable}</Link>
          ))}
        </div>
      ) : (
        <span className="relative w-[3rem] sm:w-[3.5rem] h-[3rem] sm:h-[3.5rem]">
        <Image src={Burgr} layout="fill" objectFit="cover" />
      </span>
      )} */}
    </div>
  );
};

export default Header;
