import Header from "./header";
import Image from "next/image";
// import gr1 from "@/public/images/bg1.svg";
// import gr2 from "@/public/images/gr2.png";
const Layout = ({ children }) => {
  return (
    <div className="h-[100vh] px-[2rem] sm:px-[4em] lg:px-[7.5rem] py-[2rem] sm:py-[3rem] lg:py-[3.125rem]  bg-cover mx-auto  w-full lg:max-w-[1500px]">
      <Header />
      {children}

      {/* <span className="absolute top-0 right-0 -z-50">
        <Image src={gr1} layout=" " objectFit="cover" />
      </span>
      <span className="absolute top-[20rem] lg:top-[40rem] -left-[10rem] lg:-left-[0rem] -z-50">
        <Image src={gr2} layout=" " objectFit="cover" />
      </span>
      <span></span> */}
    </div>
  );
};

export default Layout;
