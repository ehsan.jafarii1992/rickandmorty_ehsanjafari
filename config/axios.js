import axios from "axios";

const instance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_SERVER_BASE_URL || "https://rickandmortyapi.com/" ,  
  });


  instance.interceptors.request.use((request) => {
    return request;
  });

export default instance;
